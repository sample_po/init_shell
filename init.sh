#!/bin/sh

###########################
# $1: node version
# $2: private token
# $3: id of project
# $4: source branch
# $5: target branch
###########################

# パッケージアップデート
eval apt-get update

# sudoインストール
eval apt-get install -y sudo

# nodejsインストール
eval curl -sL https://deb.nodesource.com/setup_$1.x | sudo -E bash -
eval apt-get install -y nodejs

# npmパッケージのバージョンチェカーをインストール
eval npm i npm-check-updates
eval ./node_modules/npm-check-updates/bin/ncu

# MR作成
curl --request POST --header 'PRIVATE-TOKEN: '$2 https://gitlab.com/api/v4/projects/$3/merge_requests -H 'Content-Type: application/json' -d '{"source_branch": $4, "target_branch": $5, "title": "node_modulesアップデート確認のお願い"}'
